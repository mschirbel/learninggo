# Go - Aprenda a Programar

Foi criada pelo Google, por pessoas que estavam no projeto do C, projeto Linux e no UTF-8.

Podemos ver o curso ![aqui](https://www.youtube.com/watch?v=WiGU_ZB-u0w&list=PLCKpcjBB_VlBsxJ9IseNxFllf-UFEXOdg)

Ela veio para ser um meio termo entre Python(facilidade) e C++(performance).

Podemos usar concorrência e paralelismo no processador. E tudo com libraries, assim como o C.
Tem garbage collection e podemos usar em vários compiladores em plataformas diferentes.

É muito usada para o backend, pois trata muito de escala. É o fundamento de containers, também.