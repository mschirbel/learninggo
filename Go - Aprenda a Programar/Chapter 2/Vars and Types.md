# Variáveis, Tipos e Valores.

Primeiramente, podemos usar o Go Playground, é um site no qual podemos executar programas em Go sem instalar nada.

Para acessar, clique ![aqui](https://play.golang.org/).

A linguagem Go tem uma forma idiomática de fazer os programas. Ou seja, é uma forma de escrever o código. Não necessáriamente de sintaxe, mas de quebras de linhas, tabulação e etc.
Isso serve para organizar o código. Isso é o botão *Format*.

O botão *Import* serve para importar packages externas do Golang.

# Packages

Packages são importações de outros programas.
Por exemplo o pacote `fmt`, sempre temos a notação de `package.identificador`, por exemplo:

```golang
fmt.Println("Hello")
```

Se eu quiser conhecer mais sobre a função `Println` do *fmt*, posso ir nas docs ![aqui](https://golang.org/pkg/fmt/)
Isso funciona para qualquer outra função de qualquer library.

Poderia inclusive fazer um programa assim:

```golang
fmt.Println("Hello", "Argumento2", 1000, "Outro argumento")
```

# Funções

Perceba que posso passar diversos argumentos para o `Println`, isso acontece porque ela é uma função **variádica**, podemos perceber isso na documentação:

![](../media/vars1.png)

Uma função variádica trabalha com qualquer número de argumentos. Isso está claro pelos `...` na documentação, assim como na imagem acima.

O trecho de `interface{}` mostra que a função aceita quaisquer tipos de argumentos(como inteiros, floats, strings, structs e etc). A segunda parte, `(n int, err error)` reflete aqueles que são os retornos possíveis dessa função.

## Maneira Idiomática

No Golang não devemos deixar distrações no código, a linguagem interpreta como sujeira, por exemplo, declarar uma variável e não utilizar.

Sempre que usar o retorno de uma função, como por exemplo o `Println` que retorna um `n int, err error`, prrecisaríamos então de duas variáveis para abrigar esses retornos, como por exemplo:

```golang
nbytes, err := fmt.Println("Hello", "Argumento2", 1000, "Outro argumento")
fmt.Println(nbytes, err)
```

Entretanto, ainda assim, poderíamos não querer utilizar um desses valores, mas se o tirarmos, irá dar um erro de retorno com múltiplos valores. Para isso, usamos um `_` para indicar que não usaremos parte de um retorno de uma função:

```golang
_, err := fmt.Println("Hello", "Argumento2", 1000, "Outro argumento")
fmt.Println(err)
```
O nome disso é *blank identifier*.

# Variáveis

São objetos, com identificadores na memória, que podem representar um valor ou expressão. Existem diversos tipos de variáveis, como inteiros, strings, booleans, por exemplo.

## Operador Curto de Declaração

É o `:=`. É chamado de *Gopher*, e serve para **declarar** variável.
Mas para atribuir um valor a uma variável, usamos outro operador.

```golang
x := 10
y := "hello"

fmt.Printf("x: %v, %T\n", x, x)
fmt.Printf("y: %v, %T\n", y, y)
```

Nesse *Printf* estamos usando os parâmetros(`%v`) para valor e para tipagem(`%T`)

No operador curto, ele atribui um tipo para a variável mesmo sem declararmos.
E eu só posso usar esse operador se tiver um valor para uma **variável nova**. E isso só funciona dentro de *code-block*.

Todas as variáveis em Go tem *package-level-scope*, ou seja, elas só ficam disponíveis a partir da hierarquia de *code-block* que elas estão.

Caso a variável já exista vamos usar o Operador de Atribuição.

## Operador de Atribuição

```golang
x := 10
y := "hello"

fmt.Printf("x: %v, %T\n", x, x)
fmt.Printf("y: %v, %T\n", y, y)

x = 20
fmt.Printf("x: %v, %T\n", x, x)
```

Nesse caso temos que o valor de `x` foi alterado, mesmo a variável já existindo.

## Palavras Reservadas

Existem algumas palavras que não podem ser usadas, podemos ver quais são ![aqui](https://golang.org/ref/spec#Keywords).

Essas palavras são reservadas para identificadores do compilador, por isso que não podemos usar dentro do código.

## Expressão

É qualquer coisa que produz um resultado:

```golang
x := 10 + 10
fmt.Printf("x: %v\n", x)
```
O resultado será 20.

Podemos usar outros operadores, como de **comparação**:

```golang
x := 10 == 10
fmt.Println(x)
```
O resultado será *true*

Por produzir um resultado, mas não gerar uma ação, é uma expressão.